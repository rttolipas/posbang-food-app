export default () => ({
  drawer: false,
  loginModal: false,
  orders: [],
  discountPercentage: 0,
  selectedHistory: null
});
