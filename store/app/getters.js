export default {
  drawer: state => {
    return state.drawer;
  },
  loginModal: state => {
    return state.loginModal;
  },
  orders: state => {
    return state.orders;
  },
  discountPercentage: state => {
    return state.discountPercentage;
  },
  selectedHistory: state => {
    return state.selectedHistory;
  }
};
