export default {
  SET_DRAWER(state, data) {
    state.drawer = data;
  },
  SET_LOGIN_MODAL(state, data) {
    state.loginModal = data;
  },
  SET_ORDERS(state, data) {
    state.orders.push(data);
  },
  RESET_ORDERS(state, data) {
    state.orders = data;
  },
  SET_DISCOUNT_PERCENTAGE(state, data) {
    state.discountPercentage = data;
  },
  SET_SELECTED_HISTORY(state, data) {
    state.selectedHistory = data;
  }
};
