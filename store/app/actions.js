export default {
  setDrawer({ commit }, data) {
    commit("SET_DRAWER", data);
  },
  setLoginModal({ commit }, data) {
    commit("SET_LOGIN_MODAL", data);
  },
  setOrders({ commit }, data) {
    commit("SET_ORDERS", data);
  },
  setDiscountPercentage({ commit }, data) {
    commit("SET_DISCOUNT_PERCENTAGE", data);
  },
  setSelectedHistory({ commit }, data) {
    commit("SET_SELECTED_HISTORY", data);
  }
};
