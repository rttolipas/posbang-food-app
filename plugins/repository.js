import createRepository from "~/api/repository";

import taxes from "~/api/taxes/";
import coupons from "~/api/coupons/";
import users from "~/api/users/";

export default (ctx, inject) => {
  const initApiRepository = createRepository(ctx.$axios);

  const api = {
    users: users(ctx.$axios),
    categories: initApiRepository("/categories"),
    meals: initApiRepository("/meals"),
    products: initApiRepository("/products"),
    coupons: coupons(ctx.$axios),
    taxes: taxes(ctx.$axios),
    statuses: initApiRepository("/statuses"),
    orders: initApiRepository("/orders"),
    transactions: initApiRepository("/transactions")
  };

  inject("api", api);
};
