import Vue from "vue";
import Toasted from "vue-toasted";

Vue.use(Toasted);

// register the toast with the custom message
Vue.toasted.register(
  "_error",
  message => {
    return message;
  },
  {
    type: "error",
    icon: {
      name: "close-circle",
      after: true
    },
    className: "vuetoasted__customized"
  }
);

Vue.toasted.register(
  "_info",
  message => {
    return message;
  },
  {
    type: "info",
    icon: {
      name: "information",
      after: true
    },
    className: "vuetoasted__customized"
  }
);

Vue.toasted.register(
  "_success",
  message => {
    return message;
  },
  {
    type: "success",
    icon: {
      name: "check-circle",
      after: true
    },
    className: "vuetoasted__customized"
  }
);
