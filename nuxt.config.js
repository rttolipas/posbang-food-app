import colors from "vuetify/es5/util/colors";

require("dotenv").config();

export default {
  mode: "spa",
  server: {
    host: process.env.APP_HOST,
    port: process.env.APP_PORT
  },
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.APP_TITLE,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.APP_DESCRIPTION
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap"
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {},
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "~/plugins/axios", mode: "client" },
    { src: "~/plugins/icons", mode: "client" },
    { src: "~/plugins/bus", mode: "client" },
    { src: "~/plugins/repository", mode: "client" },
    { src: "~/plugins/timezone", mode: "client" },
    { src: "~/plugins/vue-toasted-custom", mode: "client" }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module",
    "@nuxtjs/vuetify"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "@nuxtjs/dotenv",
    "@nuxtjs/moment",
    "@nuxtjs/toast",
    "vuetify-dialog/nuxt"
  ],
  toast: {
    position: "top-center",
    duration: 5000,
    theme: "bubble",
    singleton: true,
    iconPack: "mdi",
    keepOnHover: true,
    className: "vuetoasted__customized"
  },
  vuetifyDialog: {
    property: "$dialog",
    confirm: {
      actions: {
        false: {
          text: "No",
          color: "error",
          flat: true
        },
        true: {
          text: "Yes",
          color: "primary",
          flat: true
        }
      }
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: `${process.env.API_URL}/api`
  },
  auth: {
    // Options
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/auth/login",
            method: "post",
            propertyName: "data.token"
          },
          logout: { url: "/auth/logout", method: "post" },
          user: {
            url: "/auth/user",
            method: "get",
            propertyName: "data"
          }
        },
        tokenRequired: true,
        tokenType: "Bearer"
      }
    },
    redirect: {
      login: "/",
      logout: "/",
      home: false
    },
    plugins: [{ src: "~/plugins/axios", mode: "client" }]
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    defaultAssets: {
      font: true,
      icons: "md"
    },
    icons: {
      iconfont: "md"
    },
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        light: {
          primary: "#002868",
          secondary: "#f1f1f1",
          tertiary: "#495057",
          accent: "#82B1FF",
          error: "#bf0a30",
          info: "#00d3ee",
          success: "#5cb860",
          warning: "#ffa21a"
        },
        dark: {
          primary: "#002868",
          secondary: "#f1f1f1",
          tertiary: "#495057",
          accent: "#82B1FF",
          error: "#bf0a30",
          info: "#00d3ee",
          success: "#5cb860",
          warning: "#ffa21a"
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.node = {
        console: false,
        fs: "empty",
        net: "empty",
        tls: "empty"
      };
    }
  }
};
