export default $axios => ({
  user(payload) {
    return $axios.$get(`/auth/user`, payload);
  }
});
