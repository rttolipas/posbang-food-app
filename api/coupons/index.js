export default $axios => ({
  validate(payload) {
    return $axios.$post(`/coupons/validate`, payload);
  }
});
