export default $axios => ({
  active(payload) {
    return $axios.$post(`/taxes/active`, payload);
  }
});
